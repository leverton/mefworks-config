<?php namespace mef\Config\Example;

/**
 * This example loops over all config files in the config/directory folder and
 * adds them to a single configuration object.
 *
 * This is different from the merged example in that the base key is derived
 * from the filename. Thus, there is no merging occuring, because each config
 * file is loaded with a different base key.
 *
 * If you compare directory/database.yaml with merged/database.yaml, you'll see
 * the former does not have a base key in the configuration file.
 */

require_once __DIR__ . '/../vendor/autoload.php';

use ArrayIterator;
use DirectoryIterator;
use mef\Config\AbstractConfig;
use mef\Config\Path;
use mef\Config\Exception\InvalidKeyException;
use mef\Config\FileLoader\IniFileLoader;
use mef\Config\FileLoader\JsonFileLoader;
use mef\Config\FileLoader\PhpFileLoader;
use mef\Config\FileLoader\YamlFileLoader;

class DirectoryConfig extends AbstractConfig
{
	/**
	 * @var array
	 */
	private $config = [];

	/**
	 * Constructor
	 *
	 * @param string $directory The path to the directory to scan.
	 */
	public function __construct($directory)
	{
		$loaders = [
			'ini' => new IniFileLoader,
			'json' => new JsonFileLoader,
			'php' => new PhpFileLoader,
			'yaml' => new YamlFileLoader,
		];

		foreach (new DirectoryIterator($directory) as $entry)
		{
			if ($entry->isFile() && isset($loaders[$entry->getExtension()]) === true)
			{
				$this->config[$entry->getBaseName('.' . $entry->getExtension())] = $loaders[$entry->getExtension()]->loadFile($entry->getPathname());
			}
		}
	}

	/**
	 * Return the configuration value for the given key.
	 *
	 * @param  string $key
	 *
	 * @return mixed
	 */
	public function get($key)
	{
		$parts = iterator_to_array(new Path($key));

		$key = array_shift($parts);

		if (isset($this->config[$key]) === false)
		{
			throw new InvalidKeyException($key);
		}

		$value = $this->config[$key];

		if ($parts !== [])
		{
			$value = $value->get(implode('.', $parts));
		}

		return $value;
	}

	/**
	 * Return iterator for all loaded configs.
	 *
	 * @return \Iterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->config);
	}
}

$config = new DirectoryConfig(__DIR__ . '/config/directory');

echo 'The database DSN is: ', $config['database.dsn'], PHP_EOL;
