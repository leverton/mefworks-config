<?php namespace mef\Config\Example;

/**
 * This example loops over all config files in the config/settings folder and
 * displays the database information.
 *
 * It illustrates the differences between the configuration formats, and how to
 * load them.
 */

require_once __DIR__ . '/../vendor/autoload.php';

use DirectoryIterator;
use mef\Config\FileLoader\IniFileLoader;
use mef\Config\FileLoader\JsonFileLoader;
use mef\Config\FileLoader\PhpFileLoader;
use mef\Config\FileLoader\YamlFileLoader;

$loaders = [
	'ini' => new IniFileLoader,
	'json' => new JsonFileLoader,
	'php' => new PhpFileLoader,
	'yaml' => new YamlFileLoader,
];

foreach (new DirectoryIterator(__DIR__ . '/config/settings') as $entry)
{
	if ($entry->isFile() && isset($loaders[$entry->getExtension()]) === true)
	{
		$config = $loaders[$entry->getExtension()]->loadFile($entry->getPathname());

		echo $entry->getFilename(), PHP_EOL;
		echo " - dsn: ", $config['database.dsn'], PHP_EOL;
		echo " - user: ", $config['database.user'], PHP_EOL;
		echo " - password: ", $config['database.password'], PHP_EOL;
	}
}