<?php namespace mef\Config\Example;

/**
 * This example loops over all config files in the config/settings folder and
 * displays the database information.
 *
 * It illustrates the differences between the configuration formats, and how to
 * load them.
 */

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Config\ArrayConfig;
use mef\Config\Path;

$config = new ArrayConfig([
	'foo.bar' => 42
]);

echo 'Does foo.bar exist?', PHP_EOL;

var_dump($config->exists('foo.bar'));

echo 'Does foo.bar exist when escaping?', PHP_EOL;

var_dump($config->exists(Path::escape('foo.bar')));