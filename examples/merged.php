<?php namespace mef\Config\Example;

/**
 * This example merges three YAML files into a single configuration.
 *
 * Since the latter files have priority, the loger.level from override.yaml
 * will be used. However, the logger.path will come from logger.yaml.
 */

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Config\CachedConfig;
use mef\Config\MergedConfig;
use mef\Config\FileLoader\YamlFileLoader;

$loader = new YamlFileLoader;
$config = new CachedConfig(
	new MergedConfig([
		$loader->loadFile(__DIR__ . '/config/merged/database.yaml'),
		$loader->loadFile(__DIR__ . '/config/merged/logger.yaml'),
		$loader->loadFile(__DIR__ . '/config/merged/override.yaml'),
	])
);

echo "Database DSN: ", $config['database.dsn'], PHP_EOL;
echo "Logger Path:  ", $config['logger.path'], PHP_EOL;
echo "Logger Level: ", $config['logger.level'], PHP_EOL;

echo 'Iteration:', PHP_EOL;
foreach ($config as $key => $value)
{
	echo $key, PHP_EOL;
	print_r($value);
}