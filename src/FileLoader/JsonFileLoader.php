<?php namespace mef\Config\FileLoader;

use RuntimeException;

/**
 * Load a JSON file.
 */
class JsonFileLoader extends AbstractFileContentsLoader
{
	/**
	 * Parse the contents of the string into an array.
	 *
	 * @param  string $contents The string content to be parsed
	 *
	 * @return array
	 */
	protected function parse($contents): array
	{
		$data = json_decode($contents, true);

		if (is_array($data) === false)
		{
			throw new RuntimeException("Unable to parse JSON: $contents");
		}

		return $data;
	}
}