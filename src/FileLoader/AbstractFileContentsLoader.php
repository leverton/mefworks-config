<?php namespace mef\Config\FileLoader;

use mef\Config\Exception\LoadException;
use mef\Config\ArrayConfig;

/**
 * An abstract class that loads contents from a file, and then passes it on to
 * the subclass's parse() method.
 */
abstract class AbstractFileContentsLoader implements FileLoaderInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function loadFile($filename): ArrayConfig
	{
		$contents = file_get_contents($filename);

		if ($contents === false)
		{
			throw new LoadException($filename, $this);
		}

		return new ArrayConfig($this->parse($contents));
	}

	/**
	 * Parse the contents of the string into an array.
	 *
	 * @param  string $contents The string content to be parsed
	 *
	 * @return array
	 */
	abstract protected function parse($contents): array;
}