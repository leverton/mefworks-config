<?php namespace mef\Config\FileLoader;

use RuntimeException;
use Symfony\Component\Yaml\Parser;

/**
 * Load a Yaml file.
 */
class YamlFileLoader extends AbstractFileContentsLoader
{
	/**
	 * @var \Symfony\Component\Yaml\Parser
	 */
	private $yaml;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->yaml = new Parser;
	}

	/**
	 * Parse the contents of the string into an array.
	 *
	 * @param  string $contents The string content to be parsed
	 *
	 * @return array
	 */
	protected function parse($contents): array
	{
		$data = $this->yaml->parse($contents);

		if (is_array($data) === false)
		{
			throw new RuntimeException("Unable to parse YAML: $contents");
		}

		return $data;
	}
}