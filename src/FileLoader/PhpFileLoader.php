<?php namespace mef\Config\FileLoader;

use mef\Config\Exception\LoadException;
use mef\Config\ArrayConfig;

/**
 * Load configuration from a PHP file.
 *
 * The PHP file must return an array or a callable object that returns an
 * array. If a callable object is returned, it will immediately be called.
 */
class PhpFileLoader implements FileLoaderInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function loadFile($filename): ArrayConfig
	{
		$data = include $filename;

		if ($data === false)
		{
			throw new LoadException($filename, $this);
		}

		if (is_callable($data) === true)
		{
			$data = call_user_func($data);
		}

		if (is_array($data) === false)
		{
			throw new LoadException($filename, $this);
		}

		return new ArrayConfig($data);
	}
}