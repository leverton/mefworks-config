<?php namespace mef\Config\FileLoader;

use mef\Config\ConfigInterface;

/**
 * A FileLoader loads a ConfigInterface object from a file specified by name.
 */
interface FileLoaderInterface
{
	/**
	 * Load a config file.
	 *
	 * @param  string $filename  The filename to load.
	 *
	 * @return \mef\Config\ConfigInterface
	 */
	public function loadFile($filename): ConfigInterface;
}