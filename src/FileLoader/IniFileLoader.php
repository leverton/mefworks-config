<?php namespace mef\Config\FileLoader;

use mef\Config\Exception\LoadException;
use mef\Config\ArrayConfig;

/**
 * Load an INI file.
 *
 * The file is parsed with parse_ini_file() treating sections as arrays
 * and using typed values.
 */
class IniFileLoader implements FileLoaderInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function loadFile($filename): ArrayConfig
	{
		$data = parse_ini_file($filename, true, INI_SCANNER_TYPED);

		if (is_array($data) === false)
		{
			throw new LoadException($filename, $this);
		}

		return new ArrayConfig($data);
	}
}