<?php namespace mef\Config;

use Traversable;

/**
 * A decorator for configuration that caches the output of get().
 *
 * Configuration objects should defer caching to this object instead of
 * implementing it internally.
 */
class CachedConfig extends AbstractConfig
{
	/**
	 * @var \mef\Config\ConfigInterface
	 */
	private $config;

	/**
	 * @var array
	 */
	private $cache = [];

	/**
	 * Constructor
	 *
	 * @param  \mef\Config\ConfigInterface $config
	 */
	public function __construct(ConfigInterface $config)
	{
		$this->config = $config;
	}

	/**
	 * Return configuration value based on key.
	 *
	 * @param  string $key The config key
	 *
	 * @return mixed
	 *
	 * @throws \mef\Config\Exception\InvalidKeyException
	 */
	public function get($key): mixed
	{
		$key = (string) $key;

		if (isset($this->cache[$key]) === false)
		{
			$this->cache[$key] = $this->config->get($key);
		}

		return $this->cache[$key];
	}

	/**
	 * Return iterator.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable
	{
		return $this->config->getIterator();
	}
}