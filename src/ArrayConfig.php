<?php namespace mef\Config;

use ArrayIterator;
use mef\Config\Exception\InvalidKeyException;
use Traversable;

/**
 * Implements the ConfigInterface interface for an immutable array.
 */
class ArrayConfig extends AbstractConfig
{
	const DOT_PLACEHOLDER = "\0Dot\0";

	/**
	 * @var array
	 */
	private $data;

	/**
	 * Constructor
	 *
	 * @param array $data The data to use for the configuration values.
	 */
	public function __construct(array $data)
	{
		$this->data = $data;
	}

	/**
	 * Return configuration value based on key.
	 *
	 * @param  string $key The config key
	 *
	 * @return mixed
	 *
	 * @throws \mef\Config\Exception\InvalidKeyException
	 */
	public function get($key): mixed
	{
		$key = (string) $key;

		$data = $this->data;

		foreach (new Path($key) as $subkey)
		{
			if (array_key_exists($subkey, $data) === false)
			{
				throw new InvalidKeyException($subkey);
			}

			$data = $data[$subkey];
		}

		return $data;
	}

	/**
	 * Return an iterator.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->data);
	}
}