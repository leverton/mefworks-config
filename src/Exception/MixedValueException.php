<?php namespace mef\Config\Exception;

use UnexpectedValueException;

/**
 * An exception for when a configuration value was not an array as it was
 * expected to be.
 */
class MixedValueException extends UnexpectedValueException
{
}