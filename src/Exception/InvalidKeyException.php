<?php namespace mef\Config\Exception;

use RuntimeException;

/**
 * An exception for when the configuration key is invalid.
 */
class InvalidKeyException extends RuntimeException
{
	/**
	 * @var string
	 */
	private $key;

	/**
	 * Constructor
	 *
	 * @param string $key  The name of the invalid key
	 */
	public function __construct($key)
	{
		$key = (string) $key;

		parent::__construct("$key is an invalid key");
		$this->key = $key;
	}

	/**
	 * Return the name of the invalid key.
	 *
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}
}