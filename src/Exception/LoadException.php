<?php namespace mef\Config\Exception;

use RuntimeException;
use mef\Config\FileLoader\FileLoaderInterface;

/**
 * An exception for when the the specified config file cannot be loaded.
 */
class LoadException extends RuntimeException
{
	/**
	 * @var string
	 */
	private $filename;

	/**
	 * @var \mef\Config\FileLoader\FileLoaderInterface
	 */
	private $fileLoader;

	/**
	 * Constructor
	 *
	 * @param string $filename The filename that could not be loaded.
	 * @param \mef\Config\FileLoader\FileLoaderInterface The file loader that
	 *          threw the exception.
	 */
	public function __construct($filename, FileLoaderInterface $fileLoader)
	{
		$this->filename = (string) $filename;
		$this->fileLoader = $fileLoader;

		parent::__construct("Unable to load {$this->filename}.");
	}

	/**
	 * Return the filename that could not be loaded.
	 *
	 * @return string
	 */
	public function getFilename(): string
	{
		return $this->filename;
	}

	/**
	 * Return the file loader that threw the exception.
	 *
	 * @return \mef\Config\FileLoader\FileLoaderInterface
	 */
	public function getFileLoader(): FileLoaderInterface
	{
		return $this->fileLoader;
	}
}