<?php namespace mef\Config;

use Traversable;

/**
 * Configuration objects are immutable key-value stores.
 */
interface ConfigInterface
{
	/**
	 * Return true if the specified key exists in the configuration.
	 *
	 * @param  string $key  The name of the config value
	 *
	 * @return bool
	 */
	public function exists($key): bool;

	/**
	 * Return the
	 *
	 * @param  string $key  The name of the config value
	 *
	 * @return mixed
	 *
	 * @throws \mef\Config\Exception\InvalidKeyException
	 */
	public function get($key): mixed;

	/**
	 * Return an iterator that iterates over every key in the configuration.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable;
}