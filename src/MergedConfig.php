<?php namespace mef\Config;

use mef\Config\Exception\InvalidKeyException;
use mef\Config\Exception\MixedValueException;
use Traversable;

/**
 * Merges two or more configs together into a single config.
 *
 * Associative arrays are merged recursively such that duplicate keys overwrite
 * earlier keys. Indexed arrays (any array where isset($array[0]) === true) are
 * replaced in their entirety.
 */
class MergedConfig extends AbstractConfig
{
	/**
	 * @var array
	 */
	private $configs;

	/**
	 * @var int
	 */
	private $configLength;

	/**
	 * Constructor
	 *
	 * @param array $configs  An array of objects that implement \mef\Config\ConfigInterface
	 *                        Later configs override earlier configs.
	 */
	public function __construct(array $configs)
	{
		$this->configs = array_map(
			function(ConfigInterface $config) {
				return ($config instanceof CachedConfig) ? $config : new CachedConfig($config);
			},
			array_reverse($configs)
		);

		$this->configLength = count($this->configs);
	}

	/**
	 * Return configuration value based on key.
	 *
	 * @param  string $key The config key
	 *
	 * @return mixed
	 *
	 * @throws \mef\Config\Exception\InvalidKeyException
	 */
	public function get($key): mixed
	{
		for ($i = 0; $i < $this->configLength; ++$i)
		{
			if ($this->configs[$i]->exists($key) === true)
			{
				$value = $this->configs[$i]->get($key);

				if (is_array($value) === true && isset($value[0]) === false)
				{
					$value = $this->merge($value, $key, $i);
				}

				return $value;
			}
		}

		throw new InvalidKeyException($key);
	}

	/**
	 * Merge an array with the rest of the configurations.
	 *
	 * @param  array  $base The array (with highest priority).
	 * @param  string $key  The key to merge on.
	 * @param  int    $i    The config index of the base array.
	 *
	 * @return array
	 *
	 * @throws \mef\Config\Exception\MixedValueException
	 */
	private function merge(array $base, $key, $i): array
	{
		while (++$i < $this->configLength)
		{
			if ($this->configs[$i]->exists($key) === true)
			{
				$nextValue = $this->configs[$i]->get($key);

				if (is_array($nextValue) === false)
				{
					throw new MixedValueException('The first value for ' . $key . ' was an array, but a subsequent value was a ' . gettype($nextValue) . '.');
				}

				$base = array_replace_recursive($nextValue, $base);
			}
		}

		return $base;
	}

	/**
	 * Return iterator.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable
	{
		$visited = [];

		foreach ($this->configs as $config)
		{
			foreach ($config->getIterator() as $key => $value)
			{
				if (isset($visited[$key]) === false)
				{
					yield $key => $this->get(Path::escape($key));
					$visited[$key] = true;
				}
			}
		}
	}
}