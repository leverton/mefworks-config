<?php namespace mef\Config;

use ArrayIterator;
use InvalidArgumentException;
use IteratorAggregate;
use Traversable;

/**
 * Represents a path. Each component is separated by a dot.
 */
class Path implements IteratorAggregate
{
	/**
	 * @var array
	 */
	protected $components;

	/**
	 * Escape a path component.
	 *
	 * This is necessary if a key has a dot in it. If left unescaped, it will
	 * be considered two (or more) components of a path.
	 *
	 * @param  string $path
	 *
	 * @return string
	 */
	static public function escape($path): string
	{
		return strtr($path, ['.' => '\\.', '\\' => '\\\\']);
	}

	/**
	 * Constructor
	 *
	 * @param string $path The path to iterate over.
	 */
	public function __construct($path)
	{
		$this->components = [];

		if (substr($path, -1) === '\\')
		{
			throw new InvalidArgumentException("The path cannot contain a trailing slash.");
		}

		$path .= '.';
		$len = strlen($path);

		$component = '';

		for ($i = 0; $i < $len; ++$i)
		{
			if ($path[$i] === '\\')
			{
				$component .= $path[++$i];
			}
			else if ($path[$i] !== '.')
			{
				$component .= $path[$i];
			}
			else if ($component === '')
			{
				throw new InvalidArgumentException("The path cannot contain an empty component.");
			}
			else
			{
				$this->components[] = $component;
				$component = '';
			}
		}
	}

	/**
	 * Return an iterator that iterates over every component in the path.
	 *
	 * @return \Iterator
	 */
	public function getIterator(): Traversable
	{
		return new ArrayIterator($this->components);
	}
}