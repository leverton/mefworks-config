<?php namespace mef\Config;

use ArrayAccess;
use IteratorAggregate;
use LogicException;
use mef\Config\Exception\InvalidKeyException;

/**
 * An abstract configuration class that implements ArrayAccess and the exists
 * method.
 *
 * Most configuration objects should extend this.
 */
abstract class AbstractConfig implements ArrayAccess, ConfigInterface, IteratorAggregate
{
	/**
	 * Return true if the key exists.
	 *
	 * @param  string $key The config key
	 *
	 * @return bool
	 */
	public function exists($key): bool
	{
		try
		{
			$this->get($key);
			return true;
		}
		catch (InvalidKeyException $e)
		{
			return false;
		}
	}

	/**
	 * Access the config as an array.
	 *
	 * An alias of $this->get().
	 *
	 * @param  string $offset
	 *
	 * @return mixed
	 */
	public function offsetGet($offset): mixed
	{
		return $this->get($offset);
	}

	/**
	 * Not implemented.
	 *
	 * @throws \LogicException
	 */
	public function offsetSet($offset, $key): void
	{
		throw new LogicException('The config values are read only.');
	}

	/**
	 * Access the config as an array.
	 *
	 * An alias of $this->exists().
	 *
	 * @param  string $offset
	 *
	 * @return bool
	 */

	public function offsetExists($offset): bool
	{
		return $this->exists($offset);
	}

	/**
	 * Not implemented.
	 *
	 * @throws \LogicException
	 */
	public function offsetUnset($offset): void
	{
		throw new LogicException('The config values are read only.');
	}
}