<?php namespace mef\Config\Test;

use mef\Config\AbstractConfig;

/**
 * @coversDefaultClass \mef\Config\AbstractConfig
 */
class AbstractConfigTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \mef\Config\AbstractConfig
	 */
	private $config;

	public function setUp()
	{
		$this->config = $this->getMockForAbstractClass(AbstractConfig::class);
	}

	/**
	 * @covers ::exists
	 */
	public function testExists()
	{
		$this->config->expects($this->once())->
			method('get')->
			will($this->returnValue(true));

		$this->assertTrue($this->config->exists('exists'));
	}

	/**
	 * @covers ::offsetGet
	 */
	public function testOffsetGet()
	{
		$this->config->expects($this->once())->
			method('get')->
			will($this->returnValue(true));

		$this->assertTrue($this->config['key']);
	}

	/**
	 * @covers ::offsetExists
	 */
	public function testOffsetExists()
	{
		$this->config->expects($this->once())->
			method('get')->
			will($this->returnValue(false));

		$this->assertTrue(isset($this->config['key']));
	}

	/**
	 * @covers ::offsetUnset
	 *
	 * @expectedException \LogicException
	 */
	public function testOffsetUnset()
	{
		unset($this->config['key']);
	}

	/**
	 * @covers ::offsetSet
	 *
	 * @expectedException \LogicException
	 */
	public function testOffsetSet()
	{
		$this->config['key'] = true;
	}
}