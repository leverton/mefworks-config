<?php namespace mef\Config\Test;

use mef\Config\MergedConfig;
use mef\Config\ArrayConfig;

/**
 * @coversDefaultClass \mef\Config\MergedConfig
 */
class MergedConfigTest extends \PHPUnit_Framework_TestCase
{
	private $config;

	public function setUp()
	{
		$this->config = new MergedConfig([
			new ArrayConfig([
				'exists' => 'yes',
				'numbers' => ['one' => 1],
				'list' => ['a', 'b', 'c'],
				'database' => [
					'dsn' => 'mysql:host=localhost;charset=utf8;dbname=test',
					'user' => 'john',
					'password' => '',
					'options' => ['persistent' => false]
				],
				'scalar' => 1,
			]),
			new ArrayConfig([
				'numbers' => ['two' => 2],
				'list' => ['d', 'e', 'f', 'g'],
				'database' => [
					'password' => 'foobar',
					'options' => ['ssl' => true],
				],
				'scalar' => ['key' => 'value']
			]),
		]);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->config instanceof MergedConfig);
	}

	/**
	 * @covers ::exists
	 */
	public function testDoesNotExist()
	{
		$this->assertFalse($this->config->exists('does not exist'));
	}

	/**
	 * @covers ::get
	 */
	public function testGet()
	{
		$this->assertSame('yes', $this->config->get('exists'));
	}

	/**
	 * @covers ::get
	 *
	 * @expectedException \mef\Config\Exception\InvalidKeyException
	 */
	public function testGetDoesNotExist()
	{
		$this->config->get('does not exists');
	}

	/**
	 * @covers ::get
	 * @covers ::merge
	 */
	public function testMergedArray()
	{
		$database = $this->config->get('database');

		// The different notations must always return the same data.
		$this->assertSame($database['options'], $this->config->get('database.options'));

		// The key based arrays are merged.
		$this->assertSame(2, count($database['options']));
		$this->assertSame(true, $database['options']['ssl']);
		$this->assertSame(false, $database['options']['persistent']);
	}

	/**
	 * @covers ::get
	 * @covers ::merge
	 */
	public function testMergedList()
	{
		$list = $this->config->get('list');

		// List (indexed arrays) are not merged. Instead, they are replaced.
		$this->assertSame(4, count($list));
		$this->assertSame(['d', 'e', 'f', 'g'], $list);
	}

	/**
	 * @covers ::get
	 * @covers ::merge
	 *
	 * @expectedException \mef\Config\Exception\MixedValueException
	 */
	public function testGetMixedValueExeption()
	{
		$this->config->get('scalar');
	}

	/**
	 * @covers ::getIterator
	 *
	 * @expectedException \mef\Config\Exception\MixedValueException
	 */
	public function testIterationWithError()
	{
		iterator_to_array($this->config->getIterator());
	}

	/**
	 * @covers ::getIterator
	 */
	public function testIteration()
	{
		$config = new MergedConfig([
			new ArrayConfig([
				'a' => 1,
				'b' => 2,
				'x.y' => 5,
			]),
			new ArrayConfig([
				'b' => 3,
				'c' => 4,
				'x.y' => 6,
			]),
		]);

		$iterator = iterator_to_array($config);
		asort($iterator);

		$this->assertSame($iterator, [
			'a' => 1,
			'b' => 3,
			'c' => 4,
			'x.y' => 6,
		]);
	}
}