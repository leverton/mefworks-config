<?php namespace mef\Config\Test\FileLoader;

use org\bovigo\vfs\vfsStream;
use mef\Config\ConfigInterface;
use mef\Config\FileLoader\AbstractFileContentsLoader;

/**
 * @coversDefaultClass \mef\Config\FileLoader\AbstractFileContentsLoader
 */
class AbstractFileContentsLoaderTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		vfsStream::setup('root', null, [
			'test.cfg' => 'parse me'
		]);
	}
	/**
	 * @covers ::loadFile
	 */
	public function testLoadFile()
	{
		$loader = $this->getMockForAbstractClass(AbstractFileContentsLoader::class);
		$loader->expects($this->once())->method('parse')->with('parse me')->willReturn(['key' => 'value']);

		$config = $loader->loadFile(vfsStream::url('root/test.cfg'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('value', $config->get('key'));
	}

	/**
	 * @covers ::loadFile
	 *
	 * @expectedException \mef\Config\Exception\LoadException
	 */
	public function testInvalidFile()
	{
		$loader = $this->getMockForAbstractClass(AbstractFileContentsLoader::class);

		@$loader->loadFile(vfsStream::url('root/404'));
	}
}