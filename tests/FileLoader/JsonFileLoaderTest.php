<?php namespace mef\Config\Test\FileLoader;

use org\bovigo\vfs\vfsStream;
use mef\Config\FileLoader\JsonFileLoader;
use mef\Config\ConfigInterface;

/**
 * @coversDefaultClass \mef\Config\FileLoader\JsonFileLoader
 */
class JsonFileLoaderTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \mef\Config\FileLoader\JsonFileLoader
	 */
	private $loader;

	public function setUp()
	{
		vfsStream::setup('root', null, [
			'test.json' => json_encode(['section' => ['foo' => 'bar']]),
			'invalid.json' => 'invalid'
		]);

		$this->loader = new JsonFileLoader;
	}

	/**
	 * @covers ::parse
	 */
	public function testLoadFile()
	{
		$config = $this->loader->loadFile(vfsStream::url('root/test.json'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('bar', $config->get('section.foo'));
	}

	/**
	 * @covers ::parse
	 *
	 * @expectedException \RuntimeException
	 */
	public function testInvalidFile()
	{
		$this->loader->loadFile(vfsStream::url('root/invalid.json'));
	}
}