<?php namespace mef\Config\Test\FileLoader;

use org\bovigo\vfs\vfsStream;
use mef\Config\FileLoader\PhpFileLoader;
use mef\Config\ConfigInterface;

/**
 * @coversDefaultClass \mef\Config\FileLoader\PhpFileLoader
 */
class PhpFileLoaderTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \mef\Config\FileLoader\PhpFileLoader
	 */
	private $loader;

	public function setUp()
	{
		vfsStream::setup('root', null, [
			'test.php' => '<?php return ["section" => ["foo" => "bar"]];',
			'callable.php' => '<?php return function() { return ["section" => ["foo" => "bar"]]; };',
			'scalar.php' => '<?php return 42; ?>',
		]);

		$this->loader = new PhpFileLoader;
	}

	/**
	 * @covers ::loadFile
	 */
	public function testLoadFile()
	{
		$config = $this->loader->loadFile(vfsStream::url('root/test.php'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('bar', $config->get('section.foo'));
	}

	/**
	 * @covers ::loadFile
	 */
	public function testLoadFileWithCallable()
	{
		$config = $this->loader->loadFile(vfsStream::url('root/callable.php'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('bar', $config->get('section.foo'));
	}

	/**
	 * @covers ::loadFile
	 *
	 * @expectedException \mef\Config\Exception\LoadException
	 */
	public function testLoadFileWithScalar()
	{
		$this->loader->loadFile(vfsStream::url('root/scalar.php'));
	}

	/**
	 * @covers ::loadFile
	 *
	 * @expectedException \mef\Config\Exception\LoadException
	 */
	public function testInvalidFile()
	{
		@$this->loader->loadFile(vfsStream::url('root/404'));
	}
}