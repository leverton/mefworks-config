<?php namespace mef\Config\Test\FileLoader;

use org\bovigo\vfs\vfsStream;
use mef\Config\FileLoader\IniFileLoader;
use mef\Config\ConfigInterface;

/**
 * @coversDefaultClass \mef\Config\FileLoader\IniFileLoader
 */
class IniFileLoaderTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \mef\Config\FileLoader\IniFileLoader
	 */
	private $loader;

	public function setUp()
	{
		vfsStream::setup('root', null, [
			'test.ini' => '[section]' . PHP_EOL . 'foo=bar'
		]);

		$this->loader = new IniFileLoader;
	}
	/**
	 * @covers ::loadFile
	 */
	public function testLoadFile()
	{
		$config = $this->loader->loadFile(vfsStream::url('root/test.ini'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('bar', $config->get('section.foo'));
	}

	/**
	 * @covers ::loadFile
	 *
	 * @expectedException \mef\Config\Exception\LoadException
	 */
	public function testInvalidFile()
	{
		@$this->loader->loadFile(vfsStream::url('root/404'));
	}
}