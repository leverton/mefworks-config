<?php namespace mef\Config\Test\FileLoader;

use org\bovigo\vfs\vfsStream;
use mef\Config\FileLoader\YamlFileLoader;
use mef\Config\ConfigInterface;

/**
 * @coversDefaultClass \mef\Config\FileLoader\YamlFileLoader
 */
class YamlFileLoaderTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var \mef\Config\FileLoader\YamlFileLoader
	 */
	private $loader;

	public function setUp()
	{
		vfsStream::setup('root', null, [
			'test.yaml' => "section:\n foo: bar",
			'invalid.yaml' => 'not an array'
		]);

		$this->loader = new YamlFileLoader;
	}

	/**
	 * @covers ::__construct
	 * @covers ::parse
	 */
	public function testLoadFile()
	{
		$config = $this->loader->loadFile(vfsStream::url('root/test.yaml'));

		$this->assertTrue($config instanceof ConfigInterface);
		$this->assertSame('bar', $config->get('section.foo'));
	}

	/**
	 * @covers ::parse
	 *
	 * @expectedException \RuntimeException
	 */
	public function testInvalidFile()
	{
		$this->loader->loadFile(vfsStream::url('root/invalid.yaml'));
	}
}