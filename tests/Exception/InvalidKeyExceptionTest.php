<?php namespace mef\Config\Test\Exception;

use mef\Config\Exception\InvalidKeyException;

/**
 * @coversDefaultClass \mef\Config\Exception\InvalidKeyException
 */
class InvalidKeyExceptionTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getKey
	 */
	public function testConstructor()
	{
		$exception = new InvalidKeyException('key');
		$this->assertSame('key', $exception->getKey());
	}
}