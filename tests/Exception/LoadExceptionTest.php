<?php namespace mef\Config\Test\Exception;

use mef\Config\Exception\LoadException;
use mef\Config\FileLoader\FileLoaderInterface;

/**
 * @coversDefaultClass \mef\Config\Exception\LoadException
 */
class LoadExceptionTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getFileLoader
	 * @covers ::getFilename
	 */
	public function testConstructor()
	{
		$loader = $this->getMock(FileLoaderInterface::class);

		$exception = new LoadException('filename', $loader);

		$this->assertSame('filename', $exception->getFilename());
		$this->assertSame($loader, $exception->getFileLoader());
	}
}