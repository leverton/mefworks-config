<?php namespace mef\Config\Test;

use mef\Config\Path;

/**
 * @coversDefaultClass \mef\Config\Path
 */
class PathTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @covers ::escape
	 */
	public function testEscape()
	{
		// The backslash should become \\
		// The dot should become \.
		$this->assertSame('\\\\\\.', Path::escape('\\.'));
	}

	/**
	 * @covers ::__construct
	 */
	public function testValidPath()
	{
		$this->assertSame(
			['foo.bar', 'zed'],
			iterator_to_array(new Path('foo\\.bar.zed'))
		);
	}

	/**
	 * @covers ::__construct
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testTrailingSlash()
	{
		new Path('test\\');
	}

	/**
	 * @covers ::__construct
	 *
	 * @expectedException \InvalidArgumentException
	 */
	public function testEmptyComponent()
	{
		new Path('foo..bar');
	}

	/**
	 * @covers ::getIterator
	 */
	public function testIteration()
	{
		$this->assertSame(['foo', 'bar'], iterator_to_array(new Path('foo.bar')));
	}
}