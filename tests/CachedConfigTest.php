<?php namespace mef\Config\Test;

use ArrayIterator;
use mef\Config\Exception\InvalidKeyException;
use mef\Config\CachedConfig;
use mef\Config\ConfigInterface;

/**
 * @coversDefaultClass \mef\Config\CachedConfig
 */
class CachedConfigTest extends \PHPUnit_Framework_TestCase
{
	private $config;

	private $innerConfig;

	public function setUp()
	{
		$this->innerConfig = $this->getMock(ConfigInterface::class);
		$this->config = new CachedConfig($this->innerConfig);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->config instanceof CachedConfig);
	}

	/**
	 * @covers ::get
	 */
	public function testGet()
	{
		$this->innerConfig->expects($this->once())->
			method('get')->
			will($this->returnValue('yes'));

		// get is called twice, but the inner config should only be called one time.
		$this->assertSame('yes', $this->config->get('exists'));
		$this->assertSame('yes', $this->config->get('exists'));
	}

	/**
	 * @covers ::get
	 *
	 * @expectedException \mef\Config\Exception\InvalidKeyException
	 */
	public function testGetDoesNotExist()
	{
		$this->innerConfig->expects($this->once())->
			method('get')->
			will($this->throwException(new InvalidKeyException('key')));

		$this->config->get('does not exists');
	}

	/**
	 * @covers ::getIterator
	 */
	public function testIteration()
	{
		$this->innerConfig->expects($this->once())->
			method('getIterator')->
			will($this->returnValue(new ArrayIterator([])));

		iterator_to_array($this->config->getIterator());
	}
}