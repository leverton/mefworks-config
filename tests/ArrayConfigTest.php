<?php namespace mef\Config\Test;

use mef\Config\ArrayConfig;

/**
 * @coversDefaultClass \mef\Config\ArrayConfig
 */
class ArrayConfigTest extends \PHPUnit_Framework_TestCase
{
	private $config;

	public function setUp()
	{
		$this->config = new ArrayConfig([
			'exists' => 'yes',
			'has.dots' => '.'
		]);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->config instanceof ArrayConfig);
	}

	/**
	 * @covers ::get
	 */
	public function testGet()
	{
		$this->assertSame('yes', $this->config->get('exists'));
	}

	/**
	 * @covers ::get
	 */
	public function testGetWithDot()
	{
		$this->assertFalse($this->config->exists('has.dot'));
		$this->assertTrue($this->config->exists('has\\.dots'));
		$this->assertSame('.', $this->config->get('has\\.dots'));
	}

	/**
	 * @covers ::get
	 *
	 * @expectedException \mef\Config\Exception\InvalidKeyException
	 */
	public function testGetDoesNotExist()
	{
		$this->config->get('does not exists');
	}

	/**
	 * @covers ::getIterator
	 */
	public function testIteration()
	{
		$this->assertSame(
			iterator_to_array($this->config->getIterator()), [
				'exists' => 'yes',
				'has.dots' => '.'
			]
		);
	}
}